package ru.inshakov.tm.endpoint;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.inshakov.tm.api.service.ISessionService;
import ru.inshakov.tm.api.service.IUserService;
import ru.inshakov.tm.api.service.ServiceLocator;
import ru.inshakov.tm.enumerated.Role;
import ru.inshakov.tm.dto.Session;
import ru.inshakov.tm.dto.User;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@WebService
@NoArgsConstructor
public final class AdminEndpoint extends AbstractEndpoint {

    private IUserService userService;

    private ISessionService sessionService;

    public AdminEndpoint(
            @NotNull final ServiceLocator serviceLocator,
            @NotNull final IUserService userService,
            @NotNull final ISessionService sessionService
    ) {
        super(serviceLocator);
        this.userService = userService;
        this.sessionService = sessionService;
    }

    @WebMethod
    public void removeByLogin(
            @WebParam(name = "session") final Session session, @WebParam(name = "login") final String login
    ) {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        userService.removeByLogin(login);
    }

    @WebMethod
    public User lockByLogin(
            @WebParam(name = "session") final Session session, @WebParam(name = "login") final String login
    ) {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        return userService.lockByLogin(login);
    }

    @WebMethod
    public User unlockByLogin(
            @WebParam(name = "session") final Session session, @WebParam(name = "login") final String login
    ) {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        return userService.unlockByLogin(login);
    }

    @WebMethod
    public void closeAllByUserId(
            @WebParam(name = "session") final Session session,
            @WebParam(name = "userId") final String userId
    ) {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        sessionService.closeAllByUserId(userId);
    }

    @Nullable
    @WebMethod
    public List<Session> findAllByUserId(
            @WebParam(name = "session") final Session session,
            @WebParam(name = "userId") final String userId
    ) {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        return sessionService.findAllByUserId(userId);
    }
}
