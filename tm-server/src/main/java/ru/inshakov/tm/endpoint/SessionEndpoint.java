package ru.inshakov.tm.endpoint;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import ru.inshakov.tm.api.service.ISessionService;
import ru.inshakov.tm.api.service.IUserService;
import ru.inshakov.tm.api.service.ServiceLocator;
import ru.inshakov.tm.dto.Session;
import ru.inshakov.tm.dto.User;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService
@NoArgsConstructor
public final class SessionEndpoint extends AbstractEndpoint {

    private ISessionService sessionService;

    private IUserService userService;

    public SessionEndpoint(
            @NotNull final ServiceLocator serviceLocator,
            @NotNull final ISessionService sessionService,
            @NotNull final IUserService userService
    ) {
        super(serviceLocator);
        this.sessionService = sessionService;
        this.userService = userService;
    }

    @WebMethod
    public Session open(
            @WebParam(name = "login") final String login,
            @WebParam(name = "password") final String password
    ) {
        return sessionService.open(login, password);
    }

    @WebMethod
    public void close(@WebParam(name = "session") final Session session) {
        serviceLocator.getSessionService().validate(session);
        sessionService.close(session);
    }

    @WebMethod
    public Session register(
            @WebParam(name = "login") final String login,
            @WebParam(name = "password") final String password,
            @WebParam(name = "email") final String email
    ) {
        userService.add(login, password, email);
        return sessionService.open(login, password);
    }

    @WebMethod
    public User setPassword(
            @WebParam(name = "session") final Session session, @WebParam(name = "password") final String password
    ) {
        serviceLocator.getSessionService().validate(session);
        return userService.setPassword(session.getUserId(), password);
    }

    @WebMethod
    public User updateUser(
            @WebParam(name = "session") final Session session,
            @WebParam(name = "firstName") final String firstName,
            @WebParam(name = "lastName") final String lastName,
            @WebParam(name = "middleName") final String middleName
    ) {
        serviceLocator.getSessionService().validate(session);
        return userService.updateUser(session.getUserId(), firstName, lastName, middleName);
    }

}
