package ru.inshakov.tm.api;

import ru.inshakov.tm.dto.AbstractEntity;

public interface IService<E extends AbstractEntity> extends IRepository<E> {

}
