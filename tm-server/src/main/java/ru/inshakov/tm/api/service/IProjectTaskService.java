package ru.inshakov.tm.api.service;

import ru.inshakov.tm.dto.Task;

import java.util.List;

public interface IProjectTaskService {

    List<Task> findTaskByProjectId(String userId, final String projectId);

    void bindTaskById(String userId, final String taskId, final String projectId);

    void unbindTaskById(String userId, final String taskId);

    void removeProjectById(String userId, final String projectId);

    void removeProjectByIndex(String userId, final Integer index);

    void removeProjectByName(String userId, final String name);

}
