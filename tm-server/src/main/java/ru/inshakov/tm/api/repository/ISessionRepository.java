package ru.inshakov.tm.api.repository;

import org.apache.ibatis.annotations.*;
import org.jetbrains.annotations.Nullable;
import ru.inshakov.tm.dto.Session;

import java.util.List;

public interface ISessionRepository {

    @Select("SELECT * FROM tm_session WHERE user_id = #{userId}")
    @Result(column = "user_id", property = "userId")
    List<Session> findAllByUserId(@Nullable String userId);

    @Delete("DELETE FROM tm_session WHERE user_id = #{userId}")
    void removeByUserId(@Nullable String userId);

    @Insert("INSERT INTO tm_session" +
            "(id, \"timestamp\", signature, user_id)" +
            "VALUES(#{id},#{timestamp},#{signature},#{userId})")
    void add(
            @Param("id") String id,
            @Param("timestamp") Long timestamp,
            @Param("signature") String signature,
            @Param("userId") String userId
    );

    @Update("UPDATE tm_session" +
            "SET \"timestamp\"=#{timestamp}, signature=#{signature}, user_id=#{userId} WHERE id = #{id}")
    void update(
            @Param("id") String id,
            @Param("timestamp") Long timestamp,
            @Param("signature") String signature,
            @Param("userId") String userId
    );

    @Select("SELECT * FROM tm_session")
    @Result(column = "user_id", property = "userId")
    List<Session> findAll();

    @Select("SELECT * FROM tm_session WHERE id = #{id} LIMIT 1")
    @Result(column = "user_id", property = "userId")
    Session findById(final String id);

    @Delete("DELETE * FROM tm_session")
    void clear();

    @Delete("DELETE FROM tm_session WHERE id = #{id}")
    void removeById(final String id);

}
