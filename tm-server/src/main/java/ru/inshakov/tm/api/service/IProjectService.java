package ru.inshakov.tm.api.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.inshakov.tm.api.IService;
import ru.inshakov.tm.dto.Project;

import java.util.Collection;
import java.util.List;

public interface IProjectService extends IService<Project> {

    Project findByName(final String userId, final String name);

    Project findByIndex(final String userId, final Integer index);

    void removeByName(final String userId, final String name);

    void removeByIndex(final String userId, final Integer index);

    Project updateById(final String userId, final String id, final String name, final String description);

    Project updateByIndex(final String userId, final Integer index, final String name, final String description);

    Project startById(final String userId, final String id);

    Project startByIndex(final String userId, final Integer index);

    Project startByName(final String userId, final String name);

    Project finishById(final String userId, final String id);

    Project finishByIndex(final String userId, final Integer index);

    Project finishByName(final String userId, final String name);

    Project add(String userId, String name, String description);

    @NotNull
    @SneakyThrows
    List<Project> findAll(@NotNull String userId);

    @SneakyThrows
    void addAll(@NotNull String userId, @Nullable Collection<Project> collection);

    @Nullable
    @SneakyThrows
    Project add(@NotNull String userId, @Nullable Project entity);

    @Nullable
    @SneakyThrows
    Project findById(@NotNull String userId, @Nullable String id);

    @SneakyThrows
    void clear(@NotNull String userId);

    @SneakyThrows
    void removeById(@NotNull String userId, @Nullable String id);

    @SneakyThrows
    void remove(@NotNull String userId, @Nullable Project entity);
}
