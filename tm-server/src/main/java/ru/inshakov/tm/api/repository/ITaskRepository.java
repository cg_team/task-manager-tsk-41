package ru.inshakov.tm.api.repository;

import org.apache.ibatis.annotations.*;
import ru.inshakov.tm.dto.Task;

import java.sql.Date;
import java.util.List;

public interface ITaskRepository {

    @Select("SELECT * FROM tm_task WHERE project_id = #{project_id} AND user_id = #{userId}")
    @Result(column = "user_id", property = "userId")
    @Result(column = "start_date", property = "startDate")
    @Result(column = "finish_date", property = "finishDate")
    @Result(column = "project_id", property = "projectId")
    List<Task> findAllTaskByProjectId(final String userId, final String projectId);

    @Delete("DELETE FROM tm_task WHERE project_id = #{project_id} AND user_id = #{userId}")
    void removeAllTaskByProjectId(final String userId, final String projectId);

    @Update("UPDATE tm_task SET project_id=#{project_id} WHERE id = #{id} AND user_id = #{userId}")
    void bindTaskToProjectById(final String userId, final String taskId, final String projectId);

    @Update("UPDATE tm_task SET project_id = NULL WHERE id = #{id} AND user_id = #{userId}")
    void unbindTaskById(final String userId, final String id);

    @Insert("INSERT INTO tm_task" +
            "(id, name, description, status, start_date, finish_date, created, user_id, project_id)" +
            "VALUES(#{id},#{name},#{description},#{status},#{startDate},#{finishDate},#{created}," +
            "#{userId},#{projectId})")
    void add(
            @Param("id") String id,
            @Param("name") String name,
            @Param("description") String description,
            @Param("status") String status,
            @Param("startDate") Date startDate,
            @Param("finishDate") Date finishDate,
            @Param("created") Date created,
            @Param("userId") String userId,
            @Param("projectId") String projectId
    );


    @Update("UPDATE tm_task" +
            "SET name=#{name}, description=#{description}, status=#{status}, start_date=#{startDate}," +
            "finish_date=#{finishDate}, created=#{created}, " +
            "user_id=#{userId}, project_id=#{projectId} WHERE id = #{id}")
    void update(
            @Param("id") String id,
            @Param("name") String name,
            @Param("description") String description,
            @Param("status") String status,
            @Param("startDate") Date startDate,
            @Param("finishDate") Date finishDate,
            @Param("created") Date created,
            @Param("userId") String userId,
            @Param("projectId") String projectId
    );

    @Select("SELECT * FROM tm_task WHERE id = #{id} AND user_id = #{userId} LIMIT 1")
    @Result(column = "user_id", property = "userId")
    @Result(column = "start_date", property = "startDate")
    @Result(column = "finish_date", property = "finishDate")
    @Result(column = "project_id", property = "projectId")
    Task findByIdUserId(final String userId, final String id);

    @Delete("DELETE FROM tm_task WHERE user_id = #{userId}")
    void clearByUserId(final String userId);

    @Delete("DELETE FROM tm_task WHERE id = #{id} AND user_id = #{userId}")
    void removeByIdUserId(final String userId, final String id);

    @Select("SELECT * FROM tm_task WHERE user_id = #{userId}")
    @Result(column = "user_id", property = "userId")
    @Result(column = "start_date", property = "startDate")
    @Result(column = "finish_date", property = "finishDate")
    @Result(column = "project_id", property = "projectId")
    List<Task> findAllByUserId(final String userId);

    @Select("SELECT * FROM tm_task WHERE name=#{name} AND user_id=#{userId} LIMIT 1")
    @Result(column = "user_id", property = "userId")
    @Result(column = "start_date", property = "startDate")
    @Result(column = "finish_date", property = "finishDate")
    @Result(column = "project_id", property = "projectId")
    Task findByName(final String userId, final String name);

    @Select("SELECT * FROM tm_task" + " WHERE user_id=#{userId} LIMIT 1 OFFSET #{index}")
    @Result(column = "user_id", property = "userId")
    @Result(column = "start_date", property = "startDate")
    @Result(column = "finish_date", property = "finishDate")
    @Result(column = "project_id", property = "projectId")
    Task findByIndex(final String userId, final int index);

    @Delete("DELETE FROM tm_task" + " WHERE name = #{name} AND user_id = #{userId}")
    void removeByName(final String userId, final String name);

    @Delete("DELETE FROM tm_task" + " WHERE user_id = #{userId}")
    void removeByIndex(final String userId, final int index);

    @Select("SELECT * FROM tm_task")
    @Result(column = "user_id", property = "userId")
    @Result(column = "start_date", property = "startDate")
    @Result(column = "finish_date", property = "finishDate")
    @Result(column = "project_id", property = "projectId")
    List<Task> findAll();

    @Select("SELECT * FROM tm_task WHERE id = #{id} LIMIT 1")
    @Result(column = "user_id", property = "userId")
    @Result(column = "start_date", property = "startDate")
    @Result(column = "finish_date", property = "finishDate")
    @Result(column = "project_id", property = "projectId")
    Task findById(final String id);

    @Delete("DELETE * FROM tm_task")
    void clear();

    @Delete("DELETE FROM tm_task WHERE id = #{id}")
    void removeById(final String id);

}
