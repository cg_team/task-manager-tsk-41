package ru.inshakov.tm.api;

import ru.inshakov.tm.dto.AbstractEntity;

import java.util.Collection;
import java.util.List;

public interface IRepository<E extends AbstractEntity> {

    List<E> findAll();

    void addAll(final Collection<E> collection);

    E add(final E entity);

    E findById(final String id);

    void clear();

    void removeById(final String id);

    void remove(final E entity);

}
