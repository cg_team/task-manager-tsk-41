package ru.inshakov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.inshakov.tm.api.IService;
import ru.inshakov.tm.enumerated.Role;
import ru.inshakov.tm.dto.Session;
import ru.inshakov.tm.dto.User;

import java.util.List;

public interface ISessionService extends IService<Session> {

    @Nullable
    Session open(@Nullable String login, @Nullable String password);

    User checkDataAccess(@Nullable String login, @Nullable String password);

    void validate(@NotNull Session session, Role role);

    void validate(@Nullable Session session);

    @Nullable
    Session sign(@Nullable Session session);

    void close(@Nullable Session session);

    void closeAllByUserId(@Nullable String userId);

    @Nullable List<Session> findAllByUserId(@Nullable String userId);
}
