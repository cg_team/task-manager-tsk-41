package ru.inshakov.tm.api.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.inshakov.tm.api.IService;
import ru.inshakov.tm.dto.Task;

import java.util.Collection;
import java.util.List;

public interface ITaskService extends IService<Task> {

    Task findByName(String userId, String name);

    Task findByIndex(String userId, Integer index);

    void removeByName(String userId, String name);

    void removeByIndex(String userId, Integer index);

    Task updateById(String userId, final String id, final String name, final String description);

    Task updateByIndex(String userId, final Integer index, final String name, final String description);

    Task startById(String userId, String id);

    Task startByIndex(String userId, Integer index);

    Task startByName(String userId, String name);

    Task finishById(String userId, String id);

    Task finishByIndex(String userId, Integer index);

    Task finishByName(String userId, String name);

    Task add(String userId, String name, String description);

    @NotNull
    @SneakyThrows
    List<Task> findAll(@NotNull String userId);

    @SneakyThrows
    void addAll(@NotNull String userId, @Nullable Collection<Task> collection);

    @Nullable
    @SneakyThrows
    Task add(@NotNull String userId, @Nullable Task entity);

    @Nullable
    @SneakyThrows
    Task findById(@NotNull String userId, @Nullable String id);

    @SneakyThrows
    void clear(@NotNull String userId);

    @SneakyThrows
    void removeById(@NotNull String userId, @Nullable String id);

    @SneakyThrows
    void remove(@NotNull String userId, @Nullable Task entity);
}
