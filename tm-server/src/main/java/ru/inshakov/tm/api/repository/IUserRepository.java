package ru.inshakov.tm.api.repository;

import org.apache.ibatis.annotations.*;
import ru.inshakov.tm.dto.User;

import java.util.List;

public interface IUserRepository {

    @Select("SELECT * FROM tm_user WHERE login = #{login}")
    @Result(column = "password_hash", property = "passwordHash")
    @Result(column = "first_name", property = "firstName")
    @Result(column = "last_name", property = "lastName")
    @Result(column = "middle_name", property = "middleName")
    User findByLogin(final String login);

    @Select("SELECT * FROM tm_user WHERE email = #{email}")
    @Result(column = "password_hash", property = "passwordHash")
    @Result(column = "first_name", property = "firstName")
    @Result(column = "last_name", property = "lastName")
    @Result(column = "middle_name", property = "middleName")
    User findByEmail(final String email);

    @Delete("DELETE FROM tm_user WHERE login = #{login}")
    void removeUserByLogin(final String login);

    @Insert("INSERT INTO tm_user" +
            "(id, email, login, role, locked, first_name, last_name, middle_name, password_hash)" +
            "VALUES(#{id},#{email},#{login},#{role},#{locked}," +
            "#{first_name},#{last_name},#{middle_name},#{password_hash})")
    void add(
            @Param("id") String id,
            @Param("email") String email,
            @Param("login") String login,
            @Param("role") String role,
            @Param("locked") boolean locked,
            @Param("firstName") String firstName,
            @Param("lastName") String lastName,
            @Param("middleName") String middleName,
            @Param("passwordHash") String passwordHash
    );

    @Update("UPDATE tm_user" +
            "SET email=#{email}, login=#{login}, role=#{role}, locked=#{locked}, " +
            "first_name=#{first_name}, last_name=#{last_name}, middle_name=#{middle_name} WHERE id=#{id}")
    void update(
            @Param("id") String id,
            @Param("email") String email,
            @Param("login") String login,
            @Param("role") String role,
            @Param("locked") boolean locked,
            @Param("firstName") String firstName,
            @Param("lastName") String lastName,
            @Param("middleName") String middleName,
            @Param("passwordHash") String passwordHash
    );

    @Select("SELECT * FROM tm_user")
    @Result(column = "password_hash", property = "passwordHash")
    @Result(column = "first_name", property = "firstName")
    @Result(column = "last_name", property = "lastName")
    @Result(column = "middle_name", property = "middleName")
    List<User> findAll();

    @Select("SELECT * FROM tm_user WHERE id = #{id} LIMIT 1")
    @Result(column = "password_hash", property = "passwordHash")
    @Result(column = "first_name", property = "firstName")
    @Result(column = "last_name", property = "lastName")
    @Result(column = "middle_name", property = "middleName")
    User findById(final String id);

    @Delete("DELETE * FROM tm_user")
    void clear();

    @Delete("DELETE FROM tm_user WHERE id = #{id}")
    void removeById(final String id);

}
