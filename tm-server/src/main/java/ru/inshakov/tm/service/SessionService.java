package ru.inshakov.tm.service;

import lombok.SneakyThrows;
import org.apache.ibatis.session.SqlSession;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.inshakov.tm.api.IPropertyService;
import ru.inshakov.tm.api.repository.ISessionRepository;
import ru.inshakov.tm.api.service.IConnectionService;
import ru.inshakov.tm.api.service.ISessionService;
import ru.inshakov.tm.api.service.IUserService;
import ru.inshakov.tm.enumerated.Role;
import ru.inshakov.tm.exception.empty.EmptyIdException;
import ru.inshakov.tm.exception.user.AccessDeniedException;
import ru.inshakov.tm.dto.Session;
import ru.inshakov.tm.dto.User;
import ru.inshakov.tm.util.HashUtil;

import java.util.Collection;
import java.util.List;
import java.util.Optional;

public final class SessionService extends AbstractService<Session> implements ISessionService {

    @NotNull
    private final IUserService userService;

    @NotNull
    private final IPropertyService propertyService;

    public SessionService(
            @NotNull final IConnectionService connectionService,
            @NotNull IUserService userService,
            @NotNull final IPropertyService propertyService
    ) {
        super(connectionService);
        this.userService = userService;
        this.propertyService = propertyService;
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<Session> findAll() {
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            @NotNull final ISessionRepository repository = sqlSession.getMapper(ISessionRepository.class);
            return repository.findAll();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Override
    @SneakyThrows
    public void addAll(@Nullable final Collection<Session> collection) {
        if (collection == null || collection.isEmpty()) return;
        for (Session item : collection) {
            add(item);
        }
        ;
    }

    @Nullable
    @Override
    @SneakyThrows
    public Session add(@Nullable final Session entity) {
        if (entity == null) return null;
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            @NotNull final ISessionRepository repository = sqlSession.getMapper(ISessionRepository.class);
            repository.add(
                    entity.getId(),
                    entity.getTimestamp(),
                    entity.getSignature(),
                    entity.getUserId()
            );
            sqlSession.commit();
            return entity;
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public Session findById(@Nullable final String id) {
        @NotNull final Optional<String> optionalId = Optional.ofNullable(id);
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            @NotNull final ISessionRepository repository = sqlSession.getMapper(ISessionRepository.class);
            return repository.findById(optionalId.orElseThrow(EmptyIdException::new));
        } finally {
            sqlSession.close();
        }
    }

    @Override
    @SneakyThrows
    public void clear() {
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            @NotNull final ISessionRepository repository = sqlSession.getMapper(ISessionRepository.class);
            repository.clear();
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Override
    @SneakyThrows
    public void removeById(@Nullable final String id) {
        @NotNull final Optional<String> optionalId = Optional.ofNullable(id);
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            @NotNull final ISessionRepository repository = sqlSession.getMapper(ISessionRepository.class);
            repository.removeById(optionalId.orElseThrow(EmptyIdException::new));
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Override
    @SneakyThrows
    public void remove(@Nullable final Session entity) {
        if (entity == null) return;
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            @NotNull final ISessionRepository repository = sqlSession.getMapper(ISessionRepository.class);
            repository.removeById(entity.getId());
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Override
    @SneakyThrows
    @NotNull
    public Session open(@Nullable final String login, @Nullable final String password) {
        @Nullable final User user = checkDataAccess(login, password);
        if (user == null) throw new AccessDeniedException();
        final Session session = new Session();
        session.setUserId(user.getId());
        session.setTimestamp(System.currentTimeMillis());
        @Nullable final Session resultSession = sign(session);
        add(resultSession);
        return resultSession;
    }

    @Override
    @SneakyThrows
    public User checkDataAccess(@Nullable final String login, @Nullable final String password) {
        if (login == null || login.isEmpty()) return null;
        if (password == null || password.isEmpty()) return null;
        @Nullable final User user = userService.findByLogin(login);
        if (user == null) return null;
        @Nullable final String hash = HashUtil.salt(propertyService, password);
        if (hash == null || hash.isEmpty() || user.isLocked()) return null;
        if (hash.equals(user.getPasswordHash())) {
            return user;
        } else {
            return null;
        }
    }

    @Override
    @SneakyThrows
    public void validate(@NotNull final Session session, final Role role) {
        if (role == null) throw new AccessDeniedException();
        validate(session);
        @Nullable final String userId = session.getUserId();
        @Nullable final User user = userService.findById(userId);
        if (user == null) throw new AccessDeniedException();
        if (!role.equals(user.getRole())) throw new AccessDeniedException();
    }

    @Override
    @SneakyThrows
    public void validate(@Nullable final Session session) {
        if (session == null) throw new AccessDeniedException();
        if (session.getSignature() == null || session.getSignature().isEmpty()) throw new AccessDeniedException();
        if (session.getUserId() == null || session.getUserId().isEmpty()) throw new AccessDeniedException();
        if (session.getTimestamp() == null) throw new AccessDeniedException();
        @Nullable final Session temp = session.clone();
        if (temp == null) throw new AccessDeniedException();
        @NotNull final String signatureSource = session.getSignature();
        @NotNull final String signatureTarget = sign(temp).getSignature();
        final boolean check = signatureSource.equals(signatureTarget);
        if (!check) throw new AccessDeniedException();
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            @NotNull final ISessionRepository repository = sqlSession.getMapper(ISessionRepository.class);
            if (repository.findById(session.getId()) == null) throw new AccessDeniedException();
        } finally {
            sqlSession.close();
        }

    }

    @Override
    @SneakyThrows
    @Nullable
    public Session sign(@Nullable final Session session) {
        if (session == null) return null;
        session.setSignature(null);
        @Nullable final String signature = HashUtil.sign(propertyService, session);
        session.setSignature(signature);
        return session;
    }

    @Override
    @SneakyThrows
    public void close(@Nullable final Session session) {
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            @NotNull final ISessionRepository repository = sqlSession.getMapper(ISessionRepository.class);
            repository.removeById(session.getId());
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Override
    @SneakyThrows
    public void closeAllByUserId(@Nullable final String userId) {
        if (userId == null) return;
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            @NotNull final ISessionRepository repository = sqlSession.getMapper(ISessionRepository.class);
            repository.removeByUserId(userId);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Override
    @SneakyThrows
    @Nullable
    public List<Session> findAllByUserId(@Nullable final String userId) {
        if (userId == null) return null;
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            @NotNull final ISessionRepository repository = sqlSession.getMapper(ISessionRepository.class);
            return repository.findAllByUserId(userId);
        } finally {
            sqlSession.close();
        }
    }
}
