package ru.inshakov.tm.service;

import lombok.SneakyThrows;
import org.apache.ibatis.session.SqlSession;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.inshakov.tm.api.repository.ITaskRepository;
import ru.inshakov.tm.api.service.IConnectionService;
import ru.inshakov.tm.api.service.ITaskService;
import ru.inshakov.tm.enumerated.Status;
import ru.inshakov.tm.exception.empty.EmptyIdException;
import ru.inshakov.tm.exception.empty.EmptyIndexException;
import ru.inshakov.tm.exception.empty.EmptyNameException;
import ru.inshakov.tm.exception.entity.TaskNotFoundException;
import ru.inshakov.tm.dto.Task;

import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Optional;

public final class TaskService extends AbstractService<Task> implements ITaskService {


    @NotNull
    public TaskService(@NotNull final IConnectionService connectionService) {
        super(connectionService);
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<Task> findAll() {
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            @NotNull final ITaskRepository repository = sqlSession.getMapper(ITaskRepository.class);
            return repository.findAll();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Override
    @SneakyThrows
    public void addAll(@Nullable final Collection<Task> collection) {
        if (collection == null) return;
        for (Task item : collection) {
            add(item);
        }
        ;
    }

    @Nullable
    @Override
    @SneakyThrows
    public Task add(@Nullable final Task entity) {
        if (entity == null) return null;
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            @NotNull final ITaskRepository repository = sqlSession.getMapper(ITaskRepository.class);
            repository.add(
                    entity.getId(),
                    entity.getName(),
                    entity.getDescription(),
                    entity.getStatus().toString(),
                    prepare(entity.getStartDate()),
                    prepare(entity.getFinishDate()),
                    prepare(entity.getCreated()),
                    entity.getUserId(),
                    entity.getProjectId()
            );
            sqlSession.commit();
            return entity;
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    private void update(@NotNull final Task entity, @NotNull SqlSession sqlSession) {
        @NotNull final ITaskRepository repository = sqlSession.getMapper(ITaskRepository.class);
        repository.update(
                entity.getId(),
                entity.getName(),
                entity.getDescription(),
                entity.getStatus().toString(),
                prepare(entity.getStartDate()),
                prepare(entity.getFinishDate()),
                prepare(entity.getCreated()),
                entity.getUserId(),
                entity.getProjectId()
        );
    }

    @Nullable
    @Override
    @SneakyThrows
    public Task findById(@Nullable final String id) {
        @NotNull final Optional<String> optionalId = Optional.ofNullable(id);
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            @NotNull final ITaskRepository repository = sqlSession.getMapper(ITaskRepository.class);
            return repository.findById(optionalId.orElseThrow(EmptyIdException::new));
        } finally {
            sqlSession.close();
        }
    }

    @Override
    @SneakyThrows
    public void clear() {
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            @NotNull final ITaskRepository repository = sqlSession.getMapper(ITaskRepository.class);
            repository.clear();
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Override
    @SneakyThrows
    public void removeById(@Nullable final String id) {
        @NotNull final Optional<String> optionalId = Optional.ofNullable(id);
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            @NotNull final ITaskRepository repository = sqlSession.getMapper(ITaskRepository.class);
            repository.removeById(optionalId.orElseThrow(EmptyIdException::new));
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Override
    @SneakyThrows
    public void remove(@Nullable final Task entity) {
        if (entity == null) return;
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            @NotNull final ITaskRepository repository = sqlSession.getMapper(ITaskRepository.class);
            repository.removeById(entity.getId());
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public Task findByIndex(@NotNull final String userId, @Nullable final Integer index) {
        if (index == null || index < 0) throw new EmptyIndexException();
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            @NotNull final ITaskRepository taskRepository = sqlSession.getMapper(ITaskRepository.class);
            return taskRepository.findByIndex(userId, index);
        } finally {
            sqlSession.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public Task findByName(@NotNull final String userId, @Nullable final String name) {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            @NotNull final ITaskRepository taskRepository = sqlSession.getMapper(ITaskRepository.class);
            return taskRepository.findByName(userId, name);
        } finally {
            sqlSession.close();
        }
    }

    @Override
    @SneakyThrows
    public void removeByIndex(@NotNull final String userId, @Nullable final Integer index) {
        if (index == null || index < 0) throw new EmptyIndexException();
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            @NotNull final ITaskRepository taskRepository = sqlSession.getMapper(ITaskRepository.class);
            taskRepository.removeByIndex(userId, index);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Override
    @SneakyThrows
    public void removeByName(@NotNull final String userId, @Nullable final String name) {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            @NotNull final ITaskRepository taskRepository = sqlSession.getMapper(ITaskRepository.class);
            taskRepository.removeByName(userId, name);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public Task updateById(
            @NotNull final String userId, @Nullable final String id,
            @Nullable final String name, @Nullable final String description) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            @NotNull final ITaskRepository taskRepository = sqlSession.getMapper(ITaskRepository.class);
            @NotNull final Task task = Optional.ofNullable(taskRepository.findByIdUserId(userId, id))
                    .orElseThrow(TaskNotFoundException::new);
            task.setName(name);
            task.setDescription(description);
            update(task, sqlSession);
            sqlSession.commit();
            return task;
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public Task updateByIndex(
            @NotNull final String userId, @Nullable final Integer index,
            @Nullable final String name, @Nullable final String description) {
        if (index == null || index < 0) throw new EmptyIndexException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            @NotNull final ITaskRepository taskRepository = sqlSession.getMapper(ITaskRepository.class);
            @NotNull final Task task = Optional.ofNullable(taskRepository.findByIndex(userId, index))
                    .orElseThrow(TaskNotFoundException::new);
            task.setName(name);
            task.setDescription(description);
            update(task, sqlSession);
            sqlSession.commit();
            return task;
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public Task startById(@NotNull final String userId, @Nullable final String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            @NotNull final ITaskRepository taskRepository = sqlSession.getMapper(ITaskRepository.class);
            @NotNull final Task task = Optional.ofNullable(taskRepository.findByIdUserId(userId, id))
                    .orElseThrow(TaskNotFoundException::new);
            task.setStatus(Status.IN_PROGRESS);
            task.setStartDate(new Date());
            update(task, sqlSession);
            sqlSession.commit();
            return task;
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public Task startByIndex(@NotNull final String userId, @Nullable final Integer index) {
        if (index == null || index < 0) throw new EmptyIndexException();
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            @NotNull final ITaskRepository taskRepository = sqlSession.getMapper(ITaskRepository.class);
            @NotNull final Task task = Optional.ofNullable(taskRepository.findByIndex(userId, index))
                    .orElseThrow(TaskNotFoundException::new);
            task.setStatus(Status.IN_PROGRESS);
            task.setStartDate(new Date());
            update(task, sqlSession);
            sqlSession.commit();
            return task;
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public Task startByName(@NotNull final String userId, @Nullable final String name) {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            @NotNull final ITaskRepository taskRepository = sqlSession.getMapper(ITaskRepository.class);
            @NotNull final Task task = Optional.ofNullable(taskRepository.findByName(userId, name))
                    .orElseThrow(TaskNotFoundException::new);
            task.setStatus(Status.IN_PROGRESS);
            task.setStartDate(new Date());
            update(task, sqlSession);
            sqlSession.commit();
            return task;
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public Task finishById(@NotNull final String userId, @Nullable final String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            @NotNull final ITaskRepository taskRepository = sqlSession.getMapper(ITaskRepository.class);
            @NotNull final Task task = Optional.ofNullable(taskRepository.findByIdUserId(userId, id))
                    .orElseThrow(TaskNotFoundException::new);
            task.setStatus(Status.COMPLETED);
            task.setFinishDate(new Date());
            update(task, sqlSession);
            sqlSession.commit();
            return task;
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public Task finishByIndex(@NotNull final String userId, @Nullable final Integer index) {
        if (index == null || index < 0) throw new EmptyIndexException();
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            @NotNull final ITaskRepository taskRepository = sqlSession.getMapper(ITaskRepository.class);
            @NotNull final Task task = Optional.ofNullable(taskRepository.findByIndex(userId, index))
                    .orElseThrow(TaskNotFoundException::new);
            task.setStatus(Status.COMPLETED);
            task.setFinishDate(new Date());
            update(task, sqlSession);
            sqlSession.commit();
            return task;
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public Task finishByName(@NotNull final String userId, @Nullable final String name) {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            @NotNull final ITaskRepository taskRepository = sqlSession.getMapper(ITaskRepository.class);
            @NotNull final Task task = Optional.ofNullable(taskRepository.findByName(userId, name))
                    .orElseThrow(TaskNotFoundException::new);
            task.setStatus(Status.COMPLETED);
            task.setFinishDate(new Date());
            update(task, sqlSession);
            sqlSession.commit();
            return task;
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Override
    @SneakyThrows
    @Nullable
    public Task add(@NotNull String userId, @Nullable String name, @Nullable String description) {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @Nullable final Task task = new Task(name, description);
        add(userId, task);
        return (task);
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<Task> findAll(@NotNull final String userId) {
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            @NotNull final ITaskRepository repository = sqlSession.getMapper(ITaskRepository.class);
            return repository.findAllByUserId(userId);
        } finally {
            sqlSession.close();
        }
    }

    @Override
    @SneakyThrows
    public void addAll(@NotNull final String userId, @Nullable final Collection<Task> collection) {
        if (collection == null || collection.isEmpty()) return;
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            sqlSession.getMapper(ITaskRepository.class);
            addAll(userId, collection);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public Task add(@NotNull final String userId, @Nullable final Task entity) {
        if (entity == null) return null;
        entity.setUserId(userId);
        @Nullable final Task entityResult = add(entity);
        return entityResult;
    }

    @Nullable
    @Override
    @SneakyThrows
    public Task findById(@NotNull final String userId, @Nullable final String id) {
        @NotNull final Optional<String> optionalId = Optional.ofNullable(id);
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            @NotNull final ITaskRepository repository = sqlSession.getMapper(ITaskRepository.class);
            return repository.findByIdUserId(userId, optionalId.orElseThrow(EmptyIdException::new));
        } finally {
            sqlSession.close();
        }
    }

    @Override
    @SneakyThrows
    public void clear(@NotNull final String userId) {
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            @NotNull final ITaskRepository repository = sqlSession.getMapper(ITaskRepository.class);
            repository.clearByUserId(userId);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Override
    @SneakyThrows
    public void removeById(@NotNull final String userId, @Nullable final String id) {
        @NotNull final Optional<String> optionalId = Optional.ofNullable(id);
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            @NotNull final ITaskRepository repository = sqlSession.getMapper(ITaskRepository.class);
            repository.removeByIdUserId(userId, optionalId.orElseThrow(EmptyIdException::new));
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Override
    @SneakyThrows
    public void remove(@NotNull final String userId, @Nullable final Task entity) {
        if (entity == null) return;
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            @NotNull final ITaskRepository repository = sqlSession.getMapper(ITaskRepository.class);
            repository.removeByIdUserId(userId, entity.getId());
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

}
