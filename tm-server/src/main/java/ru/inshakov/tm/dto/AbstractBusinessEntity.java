package ru.inshakov.tm.dto;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;

@Setter
@Getter
@MappedSuperclass
public abstract class AbstractBusinessEntity extends AbstractEntity {

    @Nullable
    @Column(name = "user_id")
    protected String userId;

}
