package ru.inshakov.tm.command.project;

import org.jetbrains.annotations.Nullable;
import ru.inshakov.tm.command.ProjectAbstractCommand;
import ru.inshakov.tm.endpoint.Project;
import ru.inshakov.tm.exception.entity.ProjectNotFoundException;
import ru.inshakov.tm.util.TerminalUtil;

public class ProjectUpdateByIdCommand extends ProjectAbstractCommand {
    @Override
    public String name() {
        return "project-update-by-id";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Update project by id.";
    }

    @Override
    public void execute() {
        System.out.println("Enter id");
        @Nullable final String id = TerminalUtil.nextLine();
        @Nullable final Project project = serviceLocator.getProjectEndpoint().findProjectById(getSession(), id);
        if (project == null) throw new ProjectNotFoundException();
        System.out.println("Enter name");
        @Nullable final String name = TerminalUtil.nextLine();
        System.out.println("Enter description");
        @Nullable final String description = TerminalUtil.nextLine();
        @Nullable final Project projectUpdated = serviceLocator.getProjectEndpoint()
                .updateProjectById(getSession(), id, name, description);
        if (projectUpdated == null) throw new ProjectNotFoundException();;
    }
}
