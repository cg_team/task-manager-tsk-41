package ru.inshakov.tm.command.data;

import org.jetbrains.annotations.Nullable;
import ru.inshakov.tm.command.AbstractCommand;

public class DataBinLoadCommand extends AbstractCommand {

    @Nullable

    public String name() {
        return "data-load-bin";
    }

    @Nullable

    public String arg() {
        return null;
    }

    @Nullable

    public String description() {
        return "Load binary data";
    }

    public void execute() {
        serviceLocator.getDataEndpoint().loadDataBin(getSession());
    }

}